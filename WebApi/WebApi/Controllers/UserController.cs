﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController(IUserRepo users)
        {
            Users = users;
        }
        public IUserRepo Users { get; set; }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = Users.Find(id);
            if (user == null)
            {
                return NotFound("User not found.");
            }
            return new ObjectResult(user);
        }

        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            if (user == null)
            {
                return BadRequest("Make sure that your input is correct.");
            }

            try
            {
                Users.Add(user);
                return CreatedAtRoute(new { name = user.Name, lastname = user.LastName }, user);
            }
            catch
            {
                return BadRequest("User already exists.");
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] User user)
        {
            if (user == null || user.Id != id)
            {
                return BadRequest("Make sure that your input is correct.");
            }

            var userForUpd = Users.Find(id);
            if (userForUpd == null)
            {
                return NotFound("User not found.");
            }

            try
            {
                Users.Update(user);
                return new ObjectResult(user);
            }
            catch
            {
                return BadRequest("User already exists");
            }
        }
    }
}