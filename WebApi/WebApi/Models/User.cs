﻿using System;

namespace WebApi.Models
{
    public class User : IEquatable<User>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as User);
        }

        public bool Equals(User other)
        {
            return other != null &&
                   Name == other.Name &&
                   LastName == other.LastName;
        }

        public override int GetHashCode()
        {
            var hashCode = 3523535;
            hashCode *= -23124123 + Name.GetHashCode();
            hashCode *= -23124123 + LastName.GetHashCode();
            return hashCode;
        }
    }
}
