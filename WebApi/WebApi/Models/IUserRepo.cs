﻿namespace WebApi.Models
{
    public interface IUserRepo
    {
        User Find(int id);
        void Add(User user);
        void Update(User user);
    }
}
