﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System;

namespace WebApi.Models
{
    public class UserRepo : IUserRepo
    {
        private static ConcurrentDictionary<int, User> _Users = new ConcurrentDictionary<int, User>();
        private static HashSet<int> _UsersUniqs = new HashSet<int>(); // TODO: Cleaning for changed users

        public User Find(int id)
        {
            _Users.TryGetValue(id, out User user);
            return user;
        }

        // In my vision code below can be unsafe for threads. Will try to find better solution.
        public void Add(User user)
        {
            if (_UsersUniqs.Contains(user.GetHashCode()))
                throw new ArgumentException("Tried to add user with same name and surname.");

            _UsersUniqs.Add(user.GetHashCode());
            user.Id = _Users.Count;
            _Users[user.Id] = user;
        }

        // Same as for Add method.
        public void Update(User user)
        {
            if (_UsersUniqs.Contains(user.GetHashCode()))
                throw new ArgumentException("Tried to add user with same name and surname.");

            _UsersUniqs.Add(user.GetHashCode());
            _Users[user.Id] = user;
        }
    }
}
